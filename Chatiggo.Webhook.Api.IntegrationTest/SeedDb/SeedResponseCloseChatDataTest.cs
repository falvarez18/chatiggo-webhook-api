using System;
using System.Collections.Generic;
using System.Text;
using Chatiggo.Webhook.Api.Model;
using Chatiggo.Webhook.Api.Model.Classes;

namespace Chatiggo.Webhook.Api.IntegrationTest.SeedDb
{
    public class SeedResponseCloseChatDataTest : Seed
    {
        public override void PopulateTestData(DomainContext dbContext)
        {
            dbContext.ResponseCloseChat.AddRange(GetData());
            dbContext.SaveChanges();
        }

        public static List<ResponseCloseChat> GetData()
        {
            return new List<ResponseCloseChat>
            {
                new ResponseCloseChat
                {
                    //Id = 2, 
					//Name = "ResponseCloseChat Name Test 1", 
					//Enabled = 1,
				},
                new ResponseCloseChat
                {
                    //Id = 1, 
					//Name = "ResponseCloseChat Name Test 2", 
					//Enabled = 1,
				},
                new ResponseCloseChat
                {
                    //Id = 3, 
					//Name = "ResponseCloseChat Name Test 3", 
					//Enabled = 1,
				},
            };
        }
    }
}
