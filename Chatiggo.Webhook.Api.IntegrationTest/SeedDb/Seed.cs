﻿using Chatiggo.Webhook.Api.Model;

namespace Chatiggo.Webhook.Api.IntegrationTest.SeedDb
{
    public abstract class Seed
    {
        public abstract void PopulateTestData(DomainContext dbContext);
    }
}
