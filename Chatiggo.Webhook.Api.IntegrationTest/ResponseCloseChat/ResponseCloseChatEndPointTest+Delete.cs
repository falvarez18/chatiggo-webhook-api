using Newtonsoft.Json;
using Chatiggo.Webhook.Api.IntegrationTest.SeedDb;
using Chatiggo.Webhook.Api.IntegrationTest.TestConfig;
using Chatiggo.Webhook.Api.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Chatiggo.Webhook.Api.IntegrationTest
{
    public partial class ResponseCloseChatEndPointTest : TestBase
    {
        public class Delete : ResponseCloseChatEndPointTest
        {
            private string Route = "api/ResponseCloseChat/";

            public Delete(TestApplicationFactory<FakeStartup> factory) : base(factory) { }

            [Fact]
            public async Task Should_return_Accepted_response_with_deleted_entity()
            {
                // Arrange
                Factory.PopulateDb(new SeedResponseCloseChatDataTest());

                var entityExpected = new ResponseCloseChatViewModel
                {
                    Id = 1,
                    //Name = "ResponseCloseChat Name Test 2",
                    //Enabled = true
                };

                var entityIdToDelete = 1;

                // Act
                var result = await _client.DeleteAsync(Route + entityIdToDelete);

                //Assert
                result.EnsureSuccessStatusCode();
                Assert.Equal(HttpStatusCode.Accepted, result.StatusCode);

                var entityResult = await result.Content.ReadAsStringAsync().ContinueWith(p => JsonConvert.DeserializeObject<ResponseCloseChatViewModel>(p.Result));
                Assert.NotNull(entityResult);
                Assert.Equal(entityIdToDelete, entityResult.Id);
                //Assert.Equal(entityExpected.Name, entityResult.Name);
                //Assert.Equal(entityExpected.Enabled, entityResult.Enabled);
            }

            [Theory]
            [InlineData(1)] //Not seeded entity
            [InlineData(2)]
            public async Task Should_return_400_Bad_Request(int entityIdToDelete)
            {
                // Arrange              

                // Act
                var result = await _client.DeleteAsync(Route + entityIdToDelete);

                //Assert
                Assert.Equal(HttpStatusCode.BadRequest, result.StatusCode);
            }



        }

    }
}
