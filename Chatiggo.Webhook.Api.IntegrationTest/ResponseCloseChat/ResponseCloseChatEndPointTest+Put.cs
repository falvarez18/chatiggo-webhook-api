using Newtonsoft.Json;
using Chatiggo.Webhook.Api.IntegrationTest.SeedDb;
using Chatiggo.Webhook.Api.IntegrationTest.TestConfig;
using Chatiggo.Webhook.Api.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Chatiggo.Webhook.Api.IntegrationTest
{
    public partial class ResponseCloseChatEndPointTest : TestBase
    {
        public class Put : ResponseCloseChatEndPointTest
        {

            private string Route = "api/ResponseCloseChat/";
            public Put(TestApplicationFactory<FakeStartup> factory) : base(factory) { }

            [Fact]
            public async Task Should_return_Accepted_response_with_edited_entity()
            {
                // Arrange
                Factory.PopulateDb(new SeedResponseCloseChatDataTest());

                var entityToEdit = new ResponseCloseChatViewModel
                {
                    Id = 1,
                    //Name = "ResponseCloseChat Name Test Edit 1",
                    //Enabled = false
                };

                var entityIdToEdit = 1;

                // Act
                var result = await _client.PutAsync(Route + entityIdToEdit, new StringContent(JsonConvert.SerializeObject(entityToEdit), Encoding.UTF8, "application/json"));

                //Assert
                result.EnsureSuccessStatusCode();
                Assert.Equal(HttpStatusCode.Accepted, result.StatusCode);

                var entityResult = await result.Content.ReadAsStringAsync().ContinueWith(p => JsonConvert.DeserializeObject<ResponseCloseChatViewModel>(p.Result));
                Assert.NotNull(entityResult);
                Assert.Equal(entityIdToEdit, entityResult.Id);
                //Assert.Equal(entityToEdit.Name, entityResult.Name);
                //Assert.Equal(entityToEdit.Enabled, entityResult.Enabled);
            }

            [Theory]
            [InlineData(1)] //Not seeded entity
            [InlineData(2)]
            public async Task Should_return_400_Bad_Request(int entityIdToEdit)
            {
                // Arrange

                var entityToCreate = new ResponseCloseChatViewModel
                {
                    Id = 1
                };

                // Act
                var result = await _client.PutAsync(Route + entityIdToEdit, new StringContent(JsonConvert.SerializeObject(entityToCreate), Encoding.UTF8, "application/json"));

                //Assert
                Assert.Equal(HttpStatusCode.BadRequest, result.StatusCode);
            }

        }

    }
}
