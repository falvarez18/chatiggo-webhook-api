using Newtonsoft.Json;
using Chatiggo.Webhook.Api.IntegrationTest.SeedDb;
using Chatiggo.Webhook.Api.IntegrationTest.TestConfig;
using Chatiggo.Webhook.Api.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Chatiggo.Webhook.Api.IntegrationTest
{
    public partial class ResponseCloseChatEndPointTest : TestBase
    {
        public class Post : ResponseCloseChatEndPointTest
        {

            private string Route = "api/ResponseCloseChat/";
            public Post(TestApplicationFactory<FakeStartup> factory) : base(factory) { }

            [Fact]
            public async Task Should_return_Accepted_response_and_added_entity_with_entityId_greater_than_zero()
            {
                // Arrange

                var entityToCreate = new ResponseCloseChatViewModel
                {
                    //Name = "ResponseCloseChat Name Test 1",
                    //Enabled = true,
                };



                // Act
                var result = await _client.PostAsync(Route, new StringContent(JsonConvert.SerializeObject(entityToCreate), Encoding.UTF8, "application/json"));

                //Assert
                result.EnsureSuccessStatusCode();
                Assert.Equal(HttpStatusCode.Accepted, result.StatusCode);

                var entityResult = await result.Content.ReadAsStringAsync().ContinueWith(p => JsonConvert.DeserializeObject<ResponseCloseChatViewModel>(p.Result));
                Assert.NotNull(entityResult);
                Assert.True(entityResult.Id > 0);
                //Assert.Equal(entityToCreate.Name, entityResult.Name);
                //Assert.Equal(entityToCreate.Enabled, entityResult.Enabled);
            }

            [Fact]
            public async Task Should_return_400_bad_request_error()
            {
                // Arrange
                var entityToCreate = new ResponseCloseChatViewModel { };

                // Act
                var result = await _client.PostAsync(Route, new StringContent(JsonConvert.SerializeObject(entityToCreate), Encoding.UTF8, "application/json"));

                //Assert
                Assert.Equal(HttpStatusCode.BadRequest, result.StatusCode);
            }

            [Fact]
            public async Task Should_return_400_bad_request_error_for_null_entity()
            {
                // Arrange
                ResponseCloseChatViewModel entityToCreate = null;

                // Act
                var result = await _client.PostAsync(Route, new StringContent(JsonConvert.SerializeObject(entityToCreate), Encoding.UTF8, "application/json"));

                //Assert
                Assert.Equal(HttpStatusCode.BadRequest, result.StatusCode);
            }
        }

    }
}
