using Newtonsoft.Json;
using Chatiggo.Webhook.Api.IntegrationTest.SeedDb;
using Chatiggo.Webhook.Api.IntegrationTest.TestConfig;
using Chatiggo.Webhook.Api.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Chatiggo.Webhook.Api.IntegrationTest
{
    public partial class ResponseCloseChatEndPointTest : TestBase
    {
        public class GetByFilter : ResponseCloseChatEndPointTest
        {
            private string Route = "api/ResponseCloseChat/GetByFilter/";

            public GetByFilter(TestApplicationFactory<FakeStartup> factory) : base(factory)
            {
                Factory.PopulateDb(new SeedResponseCloseChatDataTest());
            }

            private IEnumerable<ResponseCloseChatViewModel> ResponseCloseChatViewModelGetByFilter => new List<ResponseCloseChatViewModel>
            {
                new ResponseCloseChatViewModel
                {
                    Id = 2, 
					//Name = "ResponseCloseChat Name Test 1", 
					//Enabled = true,
				},
                new ResponseCloseChatViewModel
                {
                    Id = 1, 
					//Name = "ResponseCloseChat Name Test 2", 
					//Enabled = true,
				},
                new ResponseCloseChatViewModel
                {
                    Id = 3, 
					//Name = "ResponseCloseChat Name Test 3", 
					//Enabled = false,
				}
            };

            [Fact]
            public async Task Should_return_Ok_result_with_filtered_entities()
            {
                //Arrange
                var expectedCollection = ResponseCloseChatViewModelGetByFilter
                    //.Where(p=> )
                    ;
                var expectedNumberOfEntities = expectedCollection.Count();

                var defaultFilter = new ResponseCloseChatFilterViewModel
                {

                };

                //Act
                var result = await _client.PostAsync(Route, new StringContent(JsonConvert.SerializeObject(defaultFilter), Encoding.UTF8, "application/json"));

                //Assert
                result.EnsureSuccessStatusCode();
                var entitiesResult = await result.Content.ReadAsStringAsync().ContinueWith(p => JsonConvert.DeserializeObject<ResponseCloseChatViewModel[]>(p.Result));
                Assert.NotNull(entitiesResult);
                Assert.Equal(expectedNumberOfEntities, entitiesResult.Length);
                Assert.Collection(entitiesResult,
                    entityResult => Assert.Equal(expectedCollection.ElementAt(0), entitiesResult[0]),
                    entityResult => Assert.Equal(expectedCollection.ElementAt(1), entitiesResult[1]),
                    entityResult => Assert.Equal(expectedCollection.ElementAt(2), entitiesResult[2])
                    );
            }

        }

    }
}
