using Newtonsoft.Json;
using Chatiggo.Webhook.Api.IntegrationTest.SeedDb;
using Chatiggo.Webhook.Api.IntegrationTest.TestConfig;
using Chatiggo.Webhook.Api.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Chatiggo.Webhook.Api.IntegrationTest
{
    public partial class ResponseCloseChatEndPointTest : TestBase
    {
        public class Get : ResponseCloseChatEndPointTest
        {
            private string Route = "api/ResponseCloseChat/";

            public Get(TestApplicationFactory<FakeStartup> factory) : base(factory)
            {
                Factory.PopulateDb(new SeedResponseCloseChatDataTest());
            }

            private IEnumerable<ResponseCloseChatViewModel> ResponseCloseChatViewModelGetAll => new List<ResponseCloseChatViewModel>
            {
                new ResponseCloseChatViewModel
                {
                    Id = 2, 
					//Name = "ResponseCloseChat Name Test 1", 
					//Enabled = true,
				},
                new ResponseCloseChatViewModel
                {
                    Id = 1, 
					//Name = "ResponseCloseChat Name Test 2", 
					//Enabled = true,
				},
                new ResponseCloseChatViewModel
                {
                    Id = 3, 
					//Name = "ResponseCloseChat Name Test 3", 
					//Enabled = false,
				}
            };

            [Fact]
            public async Task Should_return_Ok_result_with_all_entities()
            {
                //Arrange
                var expectedCollection = ResponseCloseChatViewModelGetAll;
                var expectedNumberOfEntities = expectedCollection.Count();

                //Act
                var result = await _client.GetAsync(Route);

                //Assert
                result.EnsureSuccessStatusCode();
                var entitiesResult = await result.Content.ReadAsStringAsync().ContinueWith(p => JsonConvert.DeserializeObject<ResponseCloseChatViewModel[]>(p.Result));
                Assert.NotNull(entitiesResult);
                Assert.Equal(expectedNumberOfEntities, entitiesResult.Length);
                Assert.Collection(entitiesResult,
                    entityResult => Assert.Equal(expectedCollection.ElementAt(0), entitiesResult[0]),
                    entityResult => Assert.Equal(expectedCollection.ElementAt(1), entitiesResult[1]),
                    entityResult => Assert.Equal(expectedCollection.ElementAt(2), entitiesResult[2])
                    );

            }

            [Theory]
            [InlineData(1)]
            [InlineData(2)]
            public async Task Should_return_Ok_result_with_filtered_entity_by_id(int entityId)
            {
                //Arrange
                var expectedEntity = ResponseCloseChatViewModelGetAll.FirstOrDefault(p => p.Id == entityId);

                //Act
                var result = await _client.GetAsync(Route + entityId);

                //Assert
                result.EnsureSuccessStatusCode();
                var entitiesResult = await result.Content.ReadAsStringAsync().ContinueWith(p => JsonConvert.DeserializeObject<ResponseCloseChatViewModel>(p.Result));
                Assert.NotNull(entitiesResult);
                Assert.Equal(expectedEntity, entitiesResult);
            }

            [Theory]
            [InlineData(-1)]
            public async Task Sould_throw_404_NotFound_StatusCode(int entityId)
            {
                // Arrange

                // Act
                var result = await _client.GetAsync(Route + entityId);

                //Assert
                Assert.Equal(HttpStatusCode.NotFound, result.StatusCode);
            }

        }

    }
}
