using Chatiggo.Webhook.Api.IntegrationTest.TestConfig;
using System.Net.Http;

namespace Chatiggo.Webhook.Api.IntegrationTest
{
    public partial class ResponseCloseChatEndPointTest : TestBase
    {
        private HttpClient _client;

        public ResponseCloseChatEndPointTest(TestApplicationFactory<FakeStartup> factory) : base(factory)
        {
            _client = Factory.CreateClientWithTestAuth(TestClaimsProvider.WithUserClaims());
        }
    }
}
