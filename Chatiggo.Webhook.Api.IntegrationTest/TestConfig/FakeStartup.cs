﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Chatiggo.Webhook.Api.IntegrationTest.TestConfig
{
    public class FakeStartup : Startup
    {
        public FakeStartup(IConfiguration configuration, IWebHostEnvironment webHostingEnvironment) : base(configuration, webHostingEnvironment)
        {
        }

        protected override void ConfigureAuth(IServiceCollection services)
        {

        }


        //public override void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        //{
        //    base.Configure(app, env);          

        //}
    }
}
