﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Chatiggo.Webhook.Api.ViewModels.ClassesBase;

namespace Chatiggo.Webhook.Api.ViewModels
{
    public class InfoViewModel : BaseViewModel
    {
        public string Name { get; set; }
        public string Enviroment { get; set; }
        public string Version { get; set; }
        public string PublishDate { get; set; }
    }
}
