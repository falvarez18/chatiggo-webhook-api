﻿using Chatiggo.Webhook.Api.ViewModels.ClassesBase;
using System;
using System.Collections.Generic;

namespace Chatiggo.Webhook.Api.ViewModels
{
    public class ResponseCloseChatViewModel : IdentificableViewModel
    {
        public int idClient { get; set; }
        public long createdAt { get; set; }
        public long endedAt { get; set; }
        //public DateTime createdAtDate { get { return new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddMilliseconds(createdAt).ToLocalTime(); } }
       // public DateTime endedAtDate { get { return new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddMilliseconds(endedAt).ToLocalTime(); } }
        public string? type { get; set; }
        public string? contactName { get; set; }
        public string? state { get; set; }
        public List<MessageViewModel> messages { get; set; }
        public int fromId { get; set; }
        public FromViewModel from { get; set; }
        public string? destinationId { get; set; }
        public DestinationViewModel destination { get; set; }
        public int typificationId { get; set; }
        public TypificationViewModel typification { get; set; }
        public List<ChatLogViewModel> chatLogs { get; set; }
        public int campaignId { get; set; }
        public CampaignViewModel campaign { get; set; }
        public string? tz_offset { get; set; }
        public string? msisdn { get; set; }

    }
    public class AttachmentViewModel
    {
        public object? url { get; set; }
        public object? mimeType { get; set; }
    }

    public class MessageViewModel : IdentificableViewModel
    {

        public string? content { get; set; }
        public string? type { get; set; }
        public string? name { get; set; }
        public object createdAt { get; set; }
        public long createdAtNumber { get { return Convert.ToInt64(createdAt.ToString()); } }
        //public DateTime createdAtDate { get {return new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddMilliseconds(createdAtNumber).ToLocalTime(); } }

        public AttachmentViewModel attachment { get; set; }
    }

    public class FromViewModel : IdentificableViewModel
    {

        public string? type { get; set; }
        public string? did { get; set; }
        public string? guid { get; set; }
        public string? description { get; set; }
    }

    public class DestinationViewModel
    {
        public string? Id { get; set; }
        public string? msisdn { get; set; }
        public string? name { get; set; }
        public string? lastName { get; set; }
        public string? phone { get; set; }
        public string? email { get; set; }
        public string? campo6 { get; set; }
        public object? campo7 { get; set; }
        public object? campo8 { get; set; }
        public object? campo9 { get; set; }
        public object? campo10 { get; set; }
    }

    public class TypificationViewModel : IdentificableViewModel
    {
        public string? description { get; set; }
        public string? agentName { get; set; }
        public string? userName { get; set; }
        public object? parentKey { get; set; }
        public object? parentId { get; set; }
        public int userId { get; set; }
        public object? product { get; set; }
        public object? category { get; set; }
        public string? status { get; set; }
    }

    public class ChatLogViewModel : IdentificableViewModel
    {

        public object timestamp { get; set; }
        public long timestampNumber { get { return Convert.ToInt64(timestamp.ToString()); } }
        //public DateTime timestampDate { get { return new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddMilliseconds(timestampNumber).ToLocalTime(); } }
        public string? status { get; set; }
        public int? idMessage { get; set; }
        public int? idAgent { get; set; }
        public object? idGroup { get; set; }
    }

    public class CampaignViewModel : IdentificableViewModel
    {
        public string? name { get; set; }
    }

}
