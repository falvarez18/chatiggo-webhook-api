﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Chatiggo.Webhook.Api.Exceptions;
using Chatiggo.Webhook.Api.Services.IServices;
using Chatiggo.Webhook.Api.ViewModels;
using Microsoft.Extensions.Logging;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Dynamic;

namespace Chatiggo.Webhook.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ResponseCloseChatController : ClaimsController
    {
        private readonly IResponseCloseChatService _service;
        private ILogger<IResponseCloseChatService> _logger;
        public ResponseCloseChatController(IResponseCloseChatService service, ILogger<IResponseCloseChatService> logger)
        {
            _service = service;
            _logger = logger;
        }


        [HttpPost]
        [ProducesResponseType(typeof(ResponseCloseChatViewModel), StatusCodes.Status202Accepted)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Post([FromBody] ResponseCloseChatViewModel entityToAdd, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            return Accepted(await _service.Save(entityToAdd, cancellationToken));
        }



        //[HttpPost]
        //[ProducesResponseType(typeof(object), StatusCodes.Status202Accepted)]
        //[ProducesResponseType(StatusCodes.Status404NotFound)]
        //public async Task<IActionResult> Post([FromBody] object entityToAdd, CancellationToken cancellationToken)
        //{


        //    _logger.LogError(entityToAdd.ToString());
        //    cancellationToken.ThrowIfCancellationRequested();
        //    return Accepted(StatusCodes.Status200OK);
        //}

    }
}
       