﻿using Chatiggo.Webhook.Api.ViewModels;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Chatiggo.Webhook.Api.Services.IServices
{
    public interface IResponseCloseChatService
    {
        //Task<IQueryable<ResponseCloseChatViewModel>> GetAll(CancellationToken cancellationToken);
        //Task<IQueryable<ResponseCloseChatViewModel>> GetByFilter(ResponseCloseChatFilterViewModel filters, CancellationToken cancellationToken);
        //Task<ResponseCloseChatViewModel> GetById(int id, CancellationToken cancellationToken);
        Task<ResponseCloseChatViewModel> Save(ResponseCloseChatViewModel entityToAdd, CancellationToken cancellationToken);
        //Task<ResponseCloseChatViewModel> Update(int id, ResponseCloseChatViewModel entityToEdit, CancellationToken cancellationToken);
        //Task<ResponseCloseChatViewModel> Delete(int entityId, CancellationToken cancellationToken);
    }
}
