﻿using Chatiggo.Webhook.Api.Repositories.IRepositories;
using Chatiggo.Webhook.Api.Services.IServices;
using Chatiggo.Webhook.Api.ViewModels;
using Chatiggo.Webhook.Api.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using System.Net;

namespace Chatiggo.Webhook.Api.Services
{
    public class ResponseCloseChatService : IResponseCloseChatService
    {
        IResponseCloseChatRepository _repository;
        public ResponseCloseChatService(IResponseCloseChatRepository repository)
        {
            _repository = repository;
        }

    
        public Task<ResponseCloseChatViewModel> Save(ResponseCloseChatViewModel entityToAdd, CancellationToken cancellationToken)
        {

            entityToAdd = _repository.verifyExistingChildrens(entityToAdd, cancellationToken);
            return _repository.Save(entityToAdd, cancellationToken);
        }

        
    }
}
