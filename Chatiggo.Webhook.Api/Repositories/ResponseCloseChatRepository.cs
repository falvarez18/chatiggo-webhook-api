﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Chatiggo.Webhook.Api.Exceptions;
using Chatiggo.Webhook.Api.Model;
using Chatiggo.Webhook.Api.Model.Classes;
using Chatiggo.Webhook.Api.Repositories.IRepositories;
using Chatiggo.Webhook.Api.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace Chatiggo.Webhook.Api.Repositories
{
    public class ResponseCloseChatRepository : IResponseCloseChatRepository
    {
        private DomainContext _context;
        private IMapper _mapper;
        private ILogger<IResponseCloseChatRepository> _logger;
        public ResponseCloseChatRepository(DomainContext context, IMapper mapper, ILogger<IResponseCloseChatRepository> logger)
        {
            _context = context;
            _mapper = mapper;
            _logger = logger;
        }


        public Task<ResponseCloseChatViewModel> Save(ResponseCloseChatViewModel entityToAdd, CancellationToken cancellationToken)
        {
            try
            {
                var entityDb = _context.ResponseCloseChat.Add(_mapper.Map<ResponseCloseChat>(entityToAdd)).Entity;
                cancellationToken.ThrowIfCancellationRequested();
                _context.SaveChanges();
                //return Task.FromResult(_mapper.Map<ResponseCloseChatViewModel>(entityDb));
                return Task.FromResult(entityToAdd);
            }
            catch (Exception ex)
            {
                var json = JsonConvert.SerializeObject(entityToAdd, Formatting.Indented);
                _logger.LogError(json + "\n" + ex.InnerException);
                //return Task.FromResult(_mapper.Map<ResponseCloseChatViewModel>(null));
                throw;
            }
           
          
        }

        public ResponseCloseChatViewModel verifyExistingChildrens(ResponseCloseChatViewModel entityToAdd, CancellationToken cancellationToken)
        {
            var existResponseCloseChat = _context.ResponseCloseChat.Any(p => p.Id == entityToAdd.Id);
            if (existResponseCloseChat)
            {
                _logger.LogInformation("ResponseCloseChat: "+entityToAdd.Id+" "+"Ya esta guardado en base");
            }
            var existCampaign = _context.Campaign.Any(p => p.Id == entityToAdd.campaign.Id);
            if (existCampaign)
            {
                entityToAdd.campaignId = entityToAdd.campaign.Id;
                entityToAdd.campaign = null;
            }
            var existFrom = _context.From.Any(p => p.Id == entityToAdd.from.Id);
            if (existFrom)
            {
                entityToAdd.fromId = entityToAdd.from.Id;
                entityToAdd.from = null;
            }
            var existTypification = _context.Typification.Any(p => p.Id == entityToAdd.typification.Id);
            if (existTypification)
            {
                entityToAdd.typificationId = entityToAdd.typification.Id;
                entityToAdd.typification = null;
            }
            var existDestination= _context.Destination.Any(p => p.id == entityToAdd.destination.Id);
            if (existDestination)
            {
                entityToAdd.destinationId = entityToAdd.destination.Id;
                entityToAdd.destination = null;
            }
            foreach (var msg in entityToAdd.messages)
            {
                if (msg.attachment.mimeType == null && msg.attachment.url == null)
                    msg.attachment = null;
            }
            if (entityToAdd.destination.Id == null)
            {
                entityToAdd.msisdn = entityToAdd.destination.msisdn;
                entityToAdd.destination = null;
            }
            return entityToAdd;
        }

       


    }
}
