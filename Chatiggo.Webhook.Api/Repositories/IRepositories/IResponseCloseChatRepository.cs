﻿using Chatiggo.Webhook.Api.ViewModels;
using System.Threading.Tasks;
using System.Threading;
using System.Linq;

namespace Chatiggo.Webhook.Api.Repositories.IRepositories
{
    public interface IResponseCloseChatRepository
    {
        //Task<IQueryable<ResponseCloseChatViewModel>> GetAll(CancellationToken cancellationToken);
        //Task<IQueryable<ResponseCloseChatViewModel>> GetByFilter(ResponseCloseChatFilterViewModel filters, CancellationToken cancellationToken);
        //Task<ResponseCloseChatViewModel> GetById(int id, CancellationToken cancellationToken);
        Task<ResponseCloseChatViewModel> Save(ResponseCloseChatViewModel entityToAdd, CancellationToken cancellationToken);
        ResponseCloseChatViewModel verifyExistingChildrens(ResponseCloseChatViewModel entityToAdd, CancellationToken cancellationToken);
        //Task<ResponseCloseChatViewModel> Update(ResponseCloseChatViewModel entityToEdit, CancellationToken cancellationToken);
        //Task<ResponseCloseChatViewModel> Delete(int entityId, CancellationToken cancellationToken);
    }
}
