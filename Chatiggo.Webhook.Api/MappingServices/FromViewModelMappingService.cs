﻿using AutoMapper;
using Chatiggo.Webhook.Api.Model.Classes;
using Chatiggo.Webhook.Api.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Chatiggo.Webhook.Api.MappingServices
{
    public class FromViewModelMappingService : Profile
    {
        public FromViewModelMappingService()
        {
            CreateMap<FromViewModel, From>().ReverseMap();
        }
    }


}
