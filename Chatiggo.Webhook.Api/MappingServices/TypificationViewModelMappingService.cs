﻿using AutoMapper;
using Chatiggo.Webhook.Api.Model.Classes;
using Chatiggo.Webhook.Api.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Chatiggo.Webhook.Api.MappingServices
{
    public class TypificationViewModelMappingService : Profile
    {
        public TypificationViewModelMappingService()
        {
            CreateMap<TypificationViewModel, Typification>().ReverseMap();
        }
    }


}
