﻿using AutoMapper;
using Chatiggo.Webhook.Api.Model.Classes;
using Chatiggo.Webhook.Api.ViewModels;
using System;

namespace Chatiggo.Webhook.Api.MappingServices
{
    public class ResponseCloseChatViewModelMappingService : Profile
    {
        public ResponseCloseChatViewModelMappingService()
        {
            CreateMap<ResponseCloseChatViewModel, ResponseCloseChat>()
                .ForMember(dest=>dest.createdAt,orig=>orig.MapFrom(p=>DateTimeOffset.FromUnixTimeMilliseconds(p.createdAt).UtcDateTime))
                .ForMember(dest => dest.endedAt, orig => orig.MapFrom(p => DateTimeOffset.FromUnixTimeMilliseconds(p.endedAt).UtcDateTime))                
                //.ForMember(dest => dest.createdAt, orig => orig.MapFrom(p =>p.createdAtDate))                
                //.ForMember(dest => dest.endedAt, orig => orig.MapFrom(p => p.endedAtDate))                
                .ReverseMap();
        }
    }
}
