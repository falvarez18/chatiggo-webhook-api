﻿using AutoMapper;
using Chatiggo.Webhook.Api.Model.Classes;
using Chatiggo.Webhook.Api.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Chatiggo.Webhook.Api.MappingServices
{
    public class ChatLogViewModelMappingService : Profile
    {
        public ChatLogViewModelMappingService()
        {
            CreateMap<ChatLogViewModel, ChatLog>()
                .ForMember(dest => dest.timestamp, orig => orig.MapFrom(p => DateTimeOffset.FromUnixTimeMilliseconds(p.timestampNumber).UtcDateTime))               
                //.ForMember(dest => dest.timestamp, orig => orig.MapFrom(p => p.timestampDate))               
                .ReverseMap();
        }
    }


}
