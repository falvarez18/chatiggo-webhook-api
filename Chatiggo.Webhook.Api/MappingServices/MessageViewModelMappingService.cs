﻿using AutoMapper;
using Chatiggo.Webhook.Api.Model.Classes;
using Chatiggo.Webhook.Api.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Chatiggo.Webhook.Api.MappingServices
{
    public class MessageViewModelMappingService : Profile
    {
        public MessageViewModelMappingService()
        {
            CreateMap<MessageViewModel, Message>()
                .ForMember(dest => dest.createdAt, orig => orig.MapFrom(p => DateTimeOffset.FromUnixTimeMilliseconds(p.createdAtNumber).UtcDateTime))                 
                //.ForMember(dest => dest.createdAt, orig => orig.MapFrom(p => p.createdAtDate))                
                .ReverseMap();
        }
    }


}
