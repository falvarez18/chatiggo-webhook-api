﻿using FluentValidation;
using Chatiggo.Webhook.Api.ViewModels;

namespace Chatiggo.Webhook.Api.Validators
{
    public class ResponseCloseChatViewModelValidator : AbstractValidator<ResponseCloseChatViewModel>
    {
        public ResponseCloseChatViewModelValidator()
        {
            //Remove rule, only added for recive OK test in IntegrationTest.ResponseCloseChatViewModelValidatorEndPointTest+Post.Should_return_400_bad_request_error that test a posted object out of entity rules
            RuleFor(x => x.Id).GreaterThan(0);

            //e.g.
            //RuleFor(x => x.Id).NotNull();
            //RuleFor(x => x.Name).NotNull().NotEmpty().Length(0, 50);
            //RuleFor(x => x.Enabled).NotNull();        
        }
    }
}
