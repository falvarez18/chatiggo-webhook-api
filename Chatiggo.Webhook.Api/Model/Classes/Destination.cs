﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Chatiggo.Webhook.Api.Model.Classes
{
    public class Destination { 
    
        public string id { get; set; }
        public string msisdn { get; set; }
        public string? name { get; set; }
        public string? lastName { get; set; }
        public string? phone { get; set; }
        public string? email { get; set; }
        public string? campo6 { get; set; }
        public string? campo7 { get; set; }
        public string? campo8 { get; set; }
        public string? campo9 { get; set; }
        public string? campo10 { get; set; }
        public ICollection<ResponseCloseChat> ResponseCloseChat { get; set; }
    }
}
