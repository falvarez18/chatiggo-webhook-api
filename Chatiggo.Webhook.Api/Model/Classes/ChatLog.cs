﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Chatiggo.Webhook.Api.Model.Classes
{
    public class ChatLog : IdentifiableEntities
    {
       
        public DateTime timestamp { get; set; }
        public string status { get; set; }
        public int? idMessage { get; set; }
        public int? idAgent { get; set; }
        public int? idGroup { get; set; }
        public int ResponseCloseChatId { get; set; }
        public ResponseCloseChat ResponseCloseChat { get; set; }
    }
}
