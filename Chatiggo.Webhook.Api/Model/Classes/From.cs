﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Chatiggo.Webhook.Api.Model.Classes
{
    public class From: IdentifiableEntities
    {
        public string type { get; set; }
        public string did { get; set; }
        public string? guid { get; set; }
        public string? description { get; set; }
        public ICollection<ResponseCloseChat> ResponseCloseChat { get; set; }
    }
}
