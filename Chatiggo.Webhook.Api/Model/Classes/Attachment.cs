﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Chatiggo.Webhook.Api.Model.Classes
{
    public class Attachment
    {
        public int id { get; set; }
        public string? url { get; set; }
        public string? mimeType { get; set; }

    }
}
