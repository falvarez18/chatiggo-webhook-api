﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Chatiggo.Webhook.Api.Model.Classes
{
    public class ResponseCloseChat : IdentifiableEntities
    {
        //public ResponseCloseChat()
        //{
        //    messages = new HashSet<Message>();
        //    chatLogs = new HashSet<ChatLog>();          
        //}
        public int idClient { get; set; }
        public DateTime createdAt { get; set; }
        public DateTime endedAt { get; set; }
        public string? type { get; set; }
        public string? contactName { get; set; }
        public string? state { get; set; }
        public ICollection<Message> messages { get; set; }
        public int fromId { get; set; }
        public From from { get; set; }
        public string? destinationId { get; set; }
        public Destination destination { get; set; }
        public int typificationId { get; set; }
        public Typification typification { get; set; }
        public ICollection<ChatLog> chatLogs { get; set; }
        public int campaignId { get; set; }
        public Campaign campaign { get; set; }
        public string? tz_offset { get; set; }
        public string? msisdn { get; set; }
    }
}
