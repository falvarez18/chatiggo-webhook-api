﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Chatiggo.Webhook.Api.Model.Classes
{
    public class Message : IdentifiableEntities
    {
        public string? content { get; set; }
        public string? type { get; set; }
        public string? name { get; set; }
        public DateTime createdAt { get; set; }
        public int? attachmentId { get; set; }
        public Attachment attachment { get; set; }
        public int ResponseCloseChatId { get; set; }
        public ResponseCloseChat ResponseCloseChat { get; set; }

    }
}
