﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Chatiggo.Webhook.Api.Model.Classes
{
    public class Typification: IdentifiableEntities
    {
        public string? description { get; set; }
        public string? agentName { get; set; }
        public string? userName { get; set; }
        public string? parentKey { get; set; }
        public string? parentId { get; set; }
        public int userId { get; set; }
        public string? product { get; set; }
        public string? category { get; set; }
        public string? status { get; set; }
        public ICollection<ResponseCloseChat> ResponseCloseChat { get; set; }
    }
}
