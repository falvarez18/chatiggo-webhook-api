﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Chatiggo.Webhook.Api.Model.Classes
{
    public class Campaign : IdentifiableEntities
    {
        public string name { get; set; }
        public ICollection<ResponseCloseChat> ResponseCloseChat { get; set; }
    }
}
