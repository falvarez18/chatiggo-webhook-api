﻿using Chatiggo.Webhook.Api.Model.Classes;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Chatiggo.Webhook.Api.Model
{
    public partial class DomainContext { public DbSet<ResponseCloseChat> ResponseCloseChat { get; set; } }
    public class ResponseCloseChatFluentApi : BaseFluentApi
    {
        public override void OnClassCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ResponseCloseChat>(entity =>
            {
                entity.ToTable("ResponseCloseChat");

   //             entity.HasOne(p => p.from)
   //             .WithMany(p => p.ResponseCloseChat)
   //             .HasForeignKey(p=>p.idFrom)
   //              .OnDelete(DeleteBehavior.Restrict);

   //             entity.HasOne(p => p.typification)
   //         .WithOne(p => p.ResponseCloseChat)
   //              .OnDelete(DeleteBehavior.Restrict);

   //             entity.HasOne(p => p.campaign)
   //.WithOne(p => p.ResponseCloseChat)
   //              .OnDelete(DeleteBehavior.Restrict);



            });
        }
    }
}
