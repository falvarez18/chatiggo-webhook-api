﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Chatiggo.Webhook.Api.Model.Classes;


namespace Chatiggo.Webhook.Api.Model
{
    public partial class DomainContext : DbContext
    {
        public DomainContext()
        {
        }

        public DomainContext(DbContextOptions<DomainContext> options)
            : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionBuilder)
        {
            //local
            //optionBuilder.UseSqlServer(@"Data Source=DESKTOP-V0LUPVU\SQLEXPRESS;Initial Catalog=WebHookChatiggo;User ID=prueba;Password=prueba");
            //produccion
            optionBuilder.UseSqlServer(@"Data Source = 172.16.16.76\sql2008r2;Initial Catalog = WebHookChatiggo; User ID = apichatiggo;Password=Ap1Ch@tT1g0_95");
           
        }

        

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            BaseFluentApi.OnModelCreating(modelBuilder);
            //modelBuilder.Entity<ResponseCloseChat>();
        }
        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);

       public virtual DbSet<Attachment> Attachment { get; set; }
       public virtual DbSet<Campaign> Campaign { get; set; }
       public virtual DbSet<ChatLog> Chatlog { get; set; }
       public virtual DbSet<Destination> Destination { get; set; }
       public virtual DbSet<From> From { get; set; }
       public virtual DbSet<Message> Message { get; set; }
       public virtual DbSet<Typification> Typification { get; set; }
    }
}
