﻿using System;
using System.Net;

namespace Chatiggo.Webhook.Api.Exceptions
{
    public class EntityNotExistsChatiggoWebhookException : ChatiggoWebhookException
    {
        const string _message = "No se encontró la entidad";

        public EntityNotExistsChatiggoWebhookException() : base(HttpStatusCode.InternalServerError, _message)
        {

        }
        public EntityNotExistsChatiggoWebhookException(HttpStatusCode statusCode) : base(statusCode)
        {
        }

        public EntityNotExistsChatiggoWebhookException(HttpStatusCode statusCode, string message) : base(statusCode, message)
        {
        }

        public EntityNotExistsChatiggoWebhookException(HttpStatusCode statusCode, Exception innerException) : base(statusCode, innerException)
        {
        }
    }
}
