﻿using System;
using System.Net;

namespace Chatiggo.Webhook.Api.Exceptions
{
    public class EntityToEditNotFoundChatiggoWebhookException : ChatiggoWebhookException
    {
        const string _message = "No se encontró la entidad a editar";

        public EntityToEditNotFoundChatiggoWebhookException() : base(HttpStatusCode.BadRequest, _message)
        {

        }
        public EntityToEditNotFoundChatiggoWebhookException(HttpStatusCode statusCode) : base(statusCode, _message)
        {
        }

        public EntityToEditNotFoundChatiggoWebhookException(HttpStatusCode statusCode, string message) : base(statusCode, message)
        {
        }

        public EntityToEditNotFoundChatiggoWebhookException(HttpStatusCode statusCode, Exception innerException) : base(statusCode, innerException)
        {
        }
    }
}
