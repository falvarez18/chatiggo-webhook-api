﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;


namespace Chatiggo.Webhook.Api.Exceptions.Handler
{
    public class CustomLogHandler : DelegatingHandler
    {
        private readonly ILogger<ExceptionHandlerMiddleware> _logger;
        public CustomLogHandler(ILogger<ExceptionHandlerMiddleware> logger)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }
        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            var logMetadata = BuildRequestMetadata(request);
            var response = await base.SendAsync(request, cancellationToken);
            logMetadata = BuildResponseMetadata(logMetadata, response);
            await SendToLog(logMetadata);
            return response;
        }
        private LogMetadata BuildRequestMetadata(HttpRequestMessage request)
        {
            LogMetadata log = new LogMetadata
            {
                RequestMethod = request.Method.Method,
                RequestTimestamp = DateTime.Now,
                RequestUri = request.RequestUri.ToString()
            };
            return log;
        }
        private LogMetadata BuildResponseMetadata(LogMetadata logMetadata, HttpResponseMessage response)
        {
            logMetadata.ResponseStatusCode = response.StatusCode;
            logMetadata.ResponseTimestamp = DateTime.Now;
            logMetadata.ResponseContentType = response.Content.Headers.ContentType.MediaType;
            return logMetadata;
        }
        private async Task<bool> SendToLog(LogMetadata logMetadata)
        {
            // TODO: Write code here to store the logMetadata instance to a pre-configured log store...
            _logger.LogError("Prueba: " + logMetadata.RequestContentType);
            return true;
        }
        public class LogMetadata
        {
            public string RequestContentType { get; set; }
            public string RequestUri { get; set; }
            public string RequestMethod { get; set; }
            public DateTime? RequestTimestamp { get; set; }
            public string ResponseContentType { get; set; }
            public HttpStatusCode ResponseStatusCode { get; set; }
            public DateTime? ResponseTimestamp { get; set; }
        }

        //public static void Register(HttpConfiguration config)
        //{
        //    // Write your usual code here...
        //    config.MessageHandlers.Add(new CustomLogHandler(_logger));
        //}
    }
}
