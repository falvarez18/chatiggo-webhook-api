﻿using System.Net;

namespace Chatiggo.Webhook.Api.Exceptions
{
    public class InvalidOperationChatiggoWebhookException : ChatiggoWebhookException
    {
        public InvalidOperationChatiggoWebhookException() : base(HttpStatusCode.BadRequest)
        {
        }
    }
}
