﻿using Newtonsoft.Json.Linq;
using System;
using System.Net;

namespace Chatiggo.Webhook.Api.Exceptions
{
    public class ChatiggoWebhookException : Exception
    {
        public HttpStatusCode StatusCode { get; set; }
        public string ContentType { get; set; } = @"text/plain";

        public ChatiggoWebhookException(HttpStatusCode statusCode)
        {
            this.StatusCode = statusCode;
        }

        public ChatiggoWebhookException(HttpStatusCode statusCode, string message) : base(message)
        {
            this.StatusCode = statusCode;
        }

        public ChatiggoWebhookException(HttpStatusCode statusCode, Exception inner) : this(statusCode, inner.ToString()) { }

        public ChatiggoWebhookException(HttpStatusCode statusCode, JObject errorObject) : this(statusCode, errorObject.ToString())
        {
            this.ContentType = @"application/json";
        }
    }

}
