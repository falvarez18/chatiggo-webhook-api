﻿using System;
using System.Net;

namespace Chatiggo.Webhook.Api.Exceptions
{
    public class CryptoKeyNullChatiggoWebhookException : ChatiggoWebhookException
    {
        const string message = "Crypto Key is not defined in config file";

        public CryptoKeyNullChatiggoWebhookException() : base(HttpStatusCode.InternalServerError, message)
        {
        }

        public CryptoKeyNullChatiggoWebhookException(Exception ex) : base(HttpStatusCode.InternalServerError, ex.ToString())
        {
        }
    }
}
