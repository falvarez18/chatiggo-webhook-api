﻿using System;
using System.Net;

namespace Chatiggo.Webhook.Api.Exceptions
{
    public class EntityToDeleteNotFoundChatiggoWebhookException : ChatiggoWebhookException
    {
        const string _message = "No se encontró la entidad a eliminar";

        public EntityToDeleteNotFoundChatiggoWebhookException() : base(HttpStatusCode.BadRequest, _message)
        {

        }

        public EntityToDeleteNotFoundChatiggoWebhookException(HttpStatusCode statusCode) : base(statusCode, _message)
        {
        }

        public EntityToDeleteNotFoundChatiggoWebhookException(HttpStatusCode statusCode, string message) : base(statusCode, message)
        {
        }

        public EntityToDeleteNotFoundChatiggoWebhookException(HttpStatusCode statusCode, Exception innerException) : base(statusCode, innerException)
        {
        }
    }
}
