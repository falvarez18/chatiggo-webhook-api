﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Chatiggo.Webhook.Api.Migrations
{
    public partial class v10 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "tz_offset",
                table: "ResponseCloseChat",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "description",
                table: "From",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "tz_offset",
                table: "ResponseCloseChat");

            migrationBuilder.DropColumn(
                name: "description",
                table: "From");
        }
    }
}
