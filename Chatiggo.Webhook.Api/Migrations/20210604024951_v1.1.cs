﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Chatiggo.Webhook.Api.Migrations
{
    public partial class v11 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Message_Attachment_attachmentId",
                table: "Message");

            migrationBuilder.AlterColumn<string>(
                name: "content",
                table: "Message",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AlterColumn<int>(
                name: "attachmentId",
                table: "Message",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddForeignKey(
                name: "FK_Message_Attachment_attachmentId",
                table: "Message",
                column: "attachmentId",
                principalTable: "Attachment",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Message_Attachment_attachmentId",
                table: "Message");

            migrationBuilder.AlterColumn<string>(
                name: "content",
                table: "Message",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "attachmentId",
                table: "Message",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Message_Attachment_attachmentId",
                table: "Message",
                column: "attachmentId",
                principalTable: "Attachment",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
