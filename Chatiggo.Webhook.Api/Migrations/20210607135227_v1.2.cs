﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Chatiggo.Webhook.Api.Migrations
{
    public partial class v12 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "msisdn",
                table: "ResponseCloseChat",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "msisdn",
                table: "ResponseCloseChat");
        }
    }
}
