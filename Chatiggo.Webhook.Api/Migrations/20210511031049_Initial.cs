﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Chatiggo.Webhook.Api.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Attachment",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    url = table.Column<string>(nullable: true),
                    mimeType = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Attachment", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Campaign",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Campaign", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Destination",
                columns: table => new
                {
                    id = table.Column<string>(nullable: false),
                    msisdn = table.Column<string>(nullable: false),
                    name = table.Column<string>(nullable: false),
                    lastName = table.Column<string>(nullable: false),
                    phone = table.Column<string>(nullable: false),
                    email = table.Column<string>(nullable: true),
                    campo6 = table.Column<string>(nullable: true),
                    campo7 = table.Column<string>(nullable: true),
                    campo8 = table.Column<string>(nullable: true),
                    campo9 = table.Column<string>(nullable: true),
                    campo10 = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Destination", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "From",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    type = table.Column<string>(nullable: false),
                    did = table.Column<string>(nullable: false),
                    guid = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_From", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Typification",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    description = table.Column<string>(nullable: true),
                    agentName = table.Column<string>(nullable: true),
                    userName = table.Column<string>(nullable: true),
                    parentKey = table.Column<string>(nullable: true),
                    parentId = table.Column<string>(nullable: true),
                    userId = table.Column<int>(nullable: false),
                    product = table.Column<string>(nullable: true),
                    category = table.Column<string>(nullable: true),
                    status = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Typification", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ResponseCloseChat",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    idClient = table.Column<int>(nullable: false),
                    createdAt = table.Column<DateTime>(nullable: false),
                    endedAt = table.Column<DateTime>(nullable: false),
                    type = table.Column<string>(nullable: true),
                    contactName = table.Column<string>(nullable: true),
                    state = table.Column<string>(nullable: true),
                    fromId = table.Column<int>(nullable: false),
                    destinationId = table.Column<string>(nullable: true),
                    typificationId = table.Column<int>(nullable: false),
                    campaignId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ResponseCloseChat", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ResponseCloseChat_Campaign_campaignId",
                        column: x => x.campaignId,
                        principalTable: "Campaign",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ResponseCloseChat_Destination_destinationId",
                        column: x => x.destinationId,
                        principalTable: "Destination",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ResponseCloseChat_From_fromId",
                        column: x => x.fromId,
                        principalTable: "From",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ResponseCloseChat_Typification_typificationId",
                        column: x => x.typificationId,
                        principalTable: "Typification",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Chatlog",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    timestamp = table.Column<DateTime>(nullable: false),
                    status = table.Column<string>(nullable: false),
                    idMessage = table.Column<int>(nullable: true),
                    idAgent = table.Column<int>(nullable: true),
                    idGroup = table.Column<int>(nullable: true),
                    ResponseCloseChatId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Chatlog", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Chatlog_ResponseCloseChat_ResponseCloseChatId",
                        column: x => x.ResponseCloseChatId,
                        principalTable: "ResponseCloseChat",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Message",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    content = table.Column<string>(nullable: false),
                    type = table.Column<string>(nullable: false),
                    name = table.Column<string>(nullable: false),
                    createdAt = table.Column<DateTime>(nullable: false),
                    attachmentId = table.Column<int>(nullable: false),
                    ResponseCloseChatId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Message", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Message_ResponseCloseChat_ResponseCloseChatId",
                        column: x => x.ResponseCloseChatId,
                        principalTable: "ResponseCloseChat",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Message_Attachment_attachmentId",
                        column: x => x.attachmentId,
                        principalTable: "Attachment",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Chatlog_ResponseCloseChatId",
                table: "Chatlog",
                column: "ResponseCloseChatId");

            migrationBuilder.CreateIndex(
                name: "IX_Message_ResponseCloseChatId",
                table: "Message",
                column: "ResponseCloseChatId");

            migrationBuilder.CreateIndex(
                name: "IX_Message_attachmentId",
                table: "Message",
                column: "attachmentId");

            migrationBuilder.CreateIndex(
                name: "IX_ResponseCloseChat_campaignId",
                table: "ResponseCloseChat",
                column: "campaignId");

            migrationBuilder.CreateIndex(
                name: "IX_ResponseCloseChat_destinationId",
                table: "ResponseCloseChat",
                column: "destinationId");

            migrationBuilder.CreateIndex(
                name: "IX_ResponseCloseChat_fromId",
                table: "ResponseCloseChat",
                column: "fromId");

            migrationBuilder.CreateIndex(
                name: "IX_ResponseCloseChat_typificationId",
                table: "ResponseCloseChat",
                column: "typificationId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Chatlog");

            migrationBuilder.DropTable(
                name: "Message");

            migrationBuilder.DropTable(
                name: "ResponseCloseChat");

            migrationBuilder.DropTable(
                name: "Attachment");

            migrationBuilder.DropTable(
                name: "Campaign");

            migrationBuilder.DropTable(
                name: "Destination");

            migrationBuilder.DropTable(
                name: "From");

            migrationBuilder.DropTable(
                name: "Typification");
        }
    }
}
