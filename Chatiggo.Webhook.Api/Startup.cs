using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using AutoMapper;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using System.IO;
using Swashbuckle.AspNetCore.Swagger;
using Chatiggo.Webhook.Api.Model.Classes;
using Chatiggo.Webhook.Api.ViewModels.ClassesBase;
using Chatiggo.Webhook.Api.Exceptions;
using Chatiggo.Webhook.Api.Model;

namespace Chatiggo.Webhook.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration,
            IWebHostEnvironment webHostingEnvironment)
        {
            Configuration = configuration;
            WebHostingEnvironment = webHostingEnvironment;
        }

        public IConfiguration Configuration { get; }
        private IWebHostEnvironment WebHostingEnvironment { get; }
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().AddFluentValidation(fv => fv.RegisterValidatorsFromAssemblyContaining<BaseViewModel>());
            services.AddControllers();

            #region Cors

            services.AddCors(o => o.AddPolicy("AllowOrigin", builder =>
            {
                builder.AllowAnyOrigin()
                          .AllowAnyMethod()
                          .AllowAnyHeader();
            }));

            #endregion


            ConfigureAuth(services);

            #region Services

            GetTypes("Services", "Service").ToList().ForEach(type => services.TryAddScoped(type.Key, type.Value));

            #endregion

            #region Repositories

            GetTypes("Repositories", "Repository").ToList().ForEach(type => services.TryAddScoped(type.Key, type.Value));

            #endregion

            #region DbContext
            var conn = Configuration.GetConnectionString("DefaultConnection");
           services.AddDbContext<DomainContext>(options => options.UseSqlServer(conn));
            #endregion

            #region AutoMapper
            services.AddAutoMapper(Assembly.GetExecutingAssembly());          
            #endregion

            services.AddHttpClient();

            #region Swagger
            var version = Assembly.GetExecutingAssembly().GetName().Version.ToString(3);
            var environmentName = WebHostingEnvironment.EnvironmentName;
            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = $"Chatiggo Webhook Api ", Version = $"{version} - {environmentName}" });
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
                c.AddFluentValidationRules();
                c.AddSecurityDefinition("ChatiggoKey", new OpenApiSecurityScheme
                {
                    Name = "Authorization",
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "ChatiggoKey",
                    BearerFormat = "JWT",
                    In = ParameterLocation.Header,
                    Description = "JWT Authorization header using the ChatiggoKey scheme. \r\n\r\n Enter 'Bearer' [space] and then your token in the text input below.\r\n\r\nExample: \"Bearer 12345abcdef\"",
                });

                c.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "ChatiggoKey"
                            }
                        },
                        new string[] {}
                    }
                });

              
            });
            //services.AddOpenApiDocument(config =>
            //{
            //    config.DocumentName = route;
            //    config.PostProcess = document =>
            //    {
            //        document.Servers.Add(new OpenApiServer { Url = "https://[ENDPOINT]" });
            //    };
            //});

            #endregion        
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();
            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Chatiggo Webhook Api");
            });

            app.UseRouting();
            app.UseCors("AllowOrigin");
            app.UseExceptionHandlerMiddleware();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }


        protected virtual void ConfigureAuth(IServiceCollection services)
        {
            var audienceConfig = Configuration.GetSection("Audience");

            var signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(audienceConfig["Secret"]));
            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = signingKey,
                ValidateIssuer = true,
                ValidIssuer = audienceConfig["Iss"],
                ValidateAudience = true,
                ValidAudience = audienceConfig["Aud"],
                ValidateLifetime = true,
                ClockSkew = TimeSpan.Zero,
                RequireExpirationTime = true,
            };

            services.AddAuthentication(o =>
            {
                o.DefaultAuthenticateScheme = "ChatiggoKey";
                o.DefaultChallengeScheme = "ChatiggoKey";
            })
            .AddJwtBearer("ChatiggoKey", x =>
            {
                x.RequireHttpsMetadata = false;
                x.TokenValidationParameters = tokenValidationParameters;
            });

        }

        private IDictionary<Type, Type> GetTypes(string nameSpace, string endWith)
        {
            var res = new Dictionary<Type, Type>();
            var thisAssembly = Assembly.GetExecutingAssembly();
            var assemblyTypes = thisAssembly.GetTypes();
            foreach (var typeImplementation in assemblyTypes
                     .Where(p => p.Name.EndsWith(endWith))
                     .Where(p => !p.Name.Contains("Generic"))
                     .Where(t => string.Equals(t.Namespace, thisAssembly.GetName().Name + "." + nameSpace, StringComparison.Ordinal)))
                res.Add(assemblyTypes.FirstOrDefault(p => p.Name == "I" + typeImplementation.Name), typeImplementation);

            return res;
        }


    }
}
