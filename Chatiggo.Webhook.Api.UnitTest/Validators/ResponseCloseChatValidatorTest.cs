using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using FluentValidation.TestHelper;
using Chatiggo.Webhook.Api.Validators;
using Chatiggo.Webhook.Api.ViewModels;
using Xunit;

namespace Chatiggo.Webhook.Api.UnitTest.Validators
{
    public class ResponseCloseChatValidatorTest
    {
        protected ResponseCloseChatViewModelValidator ValidatorUnderTest;
        //protected Mock<IResponseCloseChatRepository> RepositoryMock;

        public ResponseCloseChatValidatorTest()
        {
            //RepositoryMock = new Mock<IResponseCloseChatRepository>();
            ValidatorUnderTest = new ResponseCloseChatViewModelValidator();
        }


        //Validator test example for Name property
        //public static IEnumerable<object[]> GetResponseCloseChatWithNameError()
        //{
        //    yield return new object[] { new ResponseCloseChatViewModel 
        //                                    {
        //                                        Name = null 
        //                                    } };
        //    yield return new object[] { new ResponseCloseChatViewModel 
        //                                    { 
        //                                        Name = string.Empty 
        //                                    } };
        //}

        //[Theory]
        //[MemberData(nameof(GetResponseCloseChatWithNameError))]
        //public async Task Should_Have_Validation_Error_For_Name(ResponseCloseChatViewModel entity)
        //{
        //    //Arrange

        //    //Act
        //    var result = await Task.FromResult( ValidatorUnderTest.TestValidate(entity));

        //    //Assert
        //    result.ShouldHaveValidationErrorFor(person => person.Name);
        //}


    }
}
