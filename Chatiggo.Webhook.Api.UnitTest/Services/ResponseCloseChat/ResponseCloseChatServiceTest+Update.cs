using Moq;
using Chatiggo.Webhook.Api.ViewModels;
using Chatiggo.Webhook.Api.Exceptions;
using System.Threading.Tasks;
using System.Threading;
using Xunit;
using System.Net;

namespace Chatiggo.Webhook.Api.UnitTest.Services
{
    public partial class ResponseCloseChatServiceTest
    {
        public class Update : ResponseCloseChatServiceTest
        {
            protected CancellationTokenSource CancellationTokenSource;

            public Update() : base()
            {
                CancellationTokenSource = new CancellationTokenSource();
            }

            [Fact]
            public async Task Should_return_edited_entity()
            {
                //Arrange

                var defaultEntity = GetDefaultResponseCloseChatViewModel()[0];
                var defaultEntityId = 1;
                var entityToEdit = new ResponseCloseChatViewModel
                {
                    Id = 1,
                    //Name = "ResponseCloseChat Name Test 2",
                    //Enabled = true,
                };

                var expectedEntity = new ResponseCloseChatViewModel
                {
                    Id = 1,
                    //Name = "ResponseCloseChat Name Test 2",
                    //Enabled = true,
                };


                RepositoryMock.Setup(p => p.Update(defaultEntity, CancellationTokenSource.Token))
                    .ReturnsAsync(defaultEntity)
                    .Verifiable();

                //Act

                var entityResult = await ServiceUnderTest.Update(defaultEntityId, entityToEdit, CancellationTokenSource.Token);

                //Assert
                RepositoryMock.Verify();
                Assert.Equal(expectedEntity, entityResult);
            }

            [Fact]
            public async Task Should_throw_ChatiggoWebhookException_for_entity_id_diferent_to_the_entity()
            {
                //Arrange

                var defaultEntityId = 2;
                var defaultEntity = GetDefaultResponseCloseChatViewModel()[0];

                //Act

                //Assert
                var ex = await Assert.ThrowsAsync<ChatiggoWebhookException>(async () => { await ServiceUnderTest.Update(defaultEntityId, defaultEntity, CancellationTokenSource.Token); });
                Assert.Equal(HttpStatusCode.BadRequest, ex.StatusCode);
            }
        }
    }
}
