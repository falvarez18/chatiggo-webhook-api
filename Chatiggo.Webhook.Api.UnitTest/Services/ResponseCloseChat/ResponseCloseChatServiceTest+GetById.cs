using Moq;
using Chatiggo.Webhook.Api.ViewModels;
using System.Threading.Tasks;
using System.Threading;
using Xunit;

namespace Chatiggo.Webhook.Api.UnitTest.Services
{
    public partial class ResponseCloseChatServiceTest
    {
        public class GetById : ResponseCloseChatServiceTest
        {
            protected CancellationTokenSource CancellationTokenSource;

            public GetById() : base()
            {
                CancellationTokenSource = new CancellationTokenSource();
            }

            [Fact]
            public async Task Should_return_entity_default()
            {
                //Arrange

                var defaultEntity = GetDefaultResponseCloseChatViewModel()[0];
                var defaultEntityId = 1;
                var expectedEntity = new ResponseCloseChatViewModel
                {
                    Id = 1,
                    //Name = "ResponseCloseChat Name Test 2",
                    //Enabled = true,
                };

                RepositoryMock.Setup(p => p.GetById(defaultEntityId, CancellationTokenSource.Token))
                                   .ReturnsAsync(defaultEntity)
                                   .Verifiable();

                //Act

                var entityResult = await ServiceUnderTest.GetById(defaultEntityId, CancellationTokenSource.Token);

                //Assert
                RepositoryMock.Verify();
                Assert.Equal(expectedEntity, entityResult);
            }
        }
    }
}
