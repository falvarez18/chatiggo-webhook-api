using AutoMapper.Configuration;
using Moq;
using Chatiggo.Webhook.Api.Repositories.IRepositories;
using Chatiggo.Webhook.Api.Services;
using Chatiggo.Webhook.Api.Services.IServices;
using Chatiggo.Webhook.Api.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Xunit;

namespace Chatiggo.Webhook.Api.UnitTest.Services
{
    public partial class ResponseCloseChatServiceTest
    {
        public class GetByFilter : ResponseCloseChatServiceTest
        {
            protected CancellationTokenSource CancellationTokenSource;

            public GetByFilter() : base()
            {
                CancellationTokenSource = new CancellationTokenSource();
            }

            [Fact]
            public async Task Should_return_default_filtered_entities()
            {
                //Arrange

                var defaultFilter = new ResponseCloseChatFilterViewModel
                {

                };

                var defaultEntities = GetDefaultResponseCloseChatViewModel()
                    .AsQueryable()
                    //.Where(p=> )
                    ;
                var expectedEntities = new List<ResponseCloseChatViewModel>
                {
                    new ResponseCloseChatViewModel
                    {
                        Id = 1,
                        //Name = "ResponseCloseChat Name Test 2",
                        //Enabled = true,
                    },
                    new ResponseCloseChatViewModel
                    {
                        Id = 2,
                        //Name = "ResponseCloseChat Name Test 1",
                        //Enabled = true,
                    },
                    new ResponseCloseChatViewModel
                    {
                        Id = 3,
                        //Name = "ResponseCloseChat Name Test 3",
                        //Enabled = false,
                    },
                    new ResponseCloseChatViewModel
                    {
                        Id = 4,
                        //Name = "ResponseCloseChat Name Test 4",
                        //Enabled = false,
                    }
                }.AsQueryable();

                RepositoryMock.Setup(p => p.GetByFilter(defaultFilter, CancellationTokenSource.Token))
                               .ReturnsAsync(defaultEntities)
                               .Verifiable();

                //Act

                var result = await ServiceUnderTest.GetByFilter(defaultFilter, CancellationTokenSource.Token);
                var entitiesResult = result.ToArray();
                //Assert
                RepositoryMock.Verify();
                Assert.Collection(entitiesResult,
                  entityResult => Assert.Equal(expectedEntities.ElementAt(0), entitiesResult[0]),
                  entityResult => Assert.Equal(expectedEntities.ElementAt(1), entitiesResult[1]),
                  entityResult => Assert.Equal(expectedEntities.ElementAt(2), entitiesResult[2]),
                  entityResult => Assert.Equal(expectedEntities.ElementAt(3), entitiesResult[3])
                  );
            }

        }

    }
}
