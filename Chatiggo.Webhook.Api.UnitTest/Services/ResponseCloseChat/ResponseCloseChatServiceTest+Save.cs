using AutoMapper.Configuration;
using Moq;
using Chatiggo.Webhook.Api.Repositories.IRepositories;
using Chatiggo.Webhook.Api.Services;
using Chatiggo.Webhook.Api.Services.IServices;
using Chatiggo.Webhook.Api.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Xunit;

namespace Chatiggo.Webhook.Api.UnitTest.Services
{
    public partial class ResponseCloseChatServiceTest
    {
        public class Save : ResponseCloseChatServiceTest
        {
            protected CancellationTokenSource CancellationTokenSource;

            public Save() : base()
            {
                CancellationTokenSource = new CancellationTokenSource();
            }

            [Fact]
            public async Task Should_return_added_entity()
            {
                //Arrange

                var defaultEntity = GetDefaultResponseCloseChatViewModel()[0];

                var entityToAdd = new ResponseCloseChatViewModel
                {
                    Id = 1,
                    //Name = "ResponseCloseChat Name Test 2",
                    //Enabled = true,
                };

                var expectedEntity = new ResponseCloseChatViewModel
                {
                    Id = 1,
                    //Name = "ResponseCloseChat Name Test 2",
                    //Enabled = true,
                };


                RepositoryMock.Setup(p => p.Save(defaultEntity, CancellationTokenSource.Token))
                    .ReturnsAsync(defaultEntity)
                    .Verifiable();

                //Act

                var entityResult = await ServiceUnderTest.Save(entityToAdd, CancellationTokenSource.Token);

                //Assert
                RepositoryMock.Verify();
                Assert.Equal(expectedEntity, entityResult);
            }
        }
    }
}
