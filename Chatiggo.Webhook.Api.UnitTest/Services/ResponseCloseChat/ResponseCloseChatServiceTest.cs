using AutoMapper.Configuration;
using Moq;
using Chatiggo.Webhook.Api.Repositories.IRepositories;
using Chatiggo.Webhook.Api.Services;
using Chatiggo.Webhook.Api.Services.IServices;
using Chatiggo.Webhook.Api.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Chatiggo.Webhook.Api.UnitTest.Services
{
    public partial class ResponseCloseChatServiceTest
    {
        protected IResponseCloseChatService ServiceUnderTest;
        protected Mock<IResponseCloseChatRepository> RepositoryMock;

        public ResponseCloseChatServiceTest()
        {
            RepositoryMock = new Mock<IResponseCloseChatRepository>();
            ServiceUnderTest = new ResponseCloseChatService(RepositoryMock.Object);
        }

        public List<ResponseCloseChatViewModel> GetDefaultResponseCloseChatViewModel()
        {
            return new List<ResponseCloseChatViewModel>
            {
                new ResponseCloseChatViewModel
                {
                    Id = 1,
                    //Name = "ResponseCloseChat Name Test 2",
                    //Enabled = true,
                },
                new ResponseCloseChatViewModel
                {
                    Id = 2,
                    //Name = "ResponseCloseChat Name Test 1",
                    //Enabled = true,
                },
                new ResponseCloseChatViewModel
                {
                    Id = 3,
                    //Name = "ResponseCloseChat Name Test 3",
                    //Enabled = false,
                },
                new ResponseCloseChatViewModel
                {
                    Id = 4,
                    //Name = "ResponseCloseChat Name Test 4",
                    //Enabled = false,
                }
            };
        }

    }
}
