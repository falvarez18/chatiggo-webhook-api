using AutoMapper.Configuration;
using Moq;
using Chatiggo.Webhook.Api.Repositories.IRepositories;
using Chatiggo.Webhook.Api.Services;
using Chatiggo.Webhook.Api.Services.IServices;
using Chatiggo.Webhook.Api.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Xunit;

namespace Chatiggo.Webhook.Api.UnitTest.Services
{
    public partial class ResponseCloseChatServiceTest
    {
        public class Delete : ResponseCloseChatServiceTest
        {
            protected CancellationTokenSource CancellationTokenSource;

            public Delete() : base()
            {
                CancellationTokenSource = new CancellationTokenSource();
            }

            [Fact]
            public async Task Should_return_deleted_entity()
            {
                //Arrange

                var defaultEntity = GetDefaultResponseCloseChatViewModel()[0];

                var defaultEntityId = 1;

                var expectedEntity = new ResponseCloseChatViewModel
                {
                    Id = 1,
                    //Name = "ResponseCloseChat Name Test 2",
                    //Enabled = true,
                };


                RepositoryMock.Setup(p => p.Delete(defaultEntityId, CancellationTokenSource.Token))
                    .ReturnsAsync(defaultEntity)
                    .Verifiable();

                //Act

                var entityResult = await ServiceUnderTest.Delete(defaultEntityId, CancellationTokenSource.Token);

                //Assert
                RepositoryMock.Verify();
                Assert.Equal(expectedEntity, entityResult);
            }
        }
    }
}
