using Microsoft.AspNetCore.Mvc;
using Moq;
using Chatiggo.Webhook.Api.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using Xunit;

namespace Chatiggo.Webhook.Api.UnitTest.Controllers
{
    public partial class ResponseCloseChatControllerTest
    {
        public class GetByFilter : ResponseCloseChatControllerTest
        {
            protected CancellationTokenSource CancellationTokenSource;

            public GetByFilter() : base()
            {
                CancellationTokenSource = new CancellationTokenSource();
            }

            [Fact]
            public async Task Should_return_Ok_result_with_filtered_entities()
            {
                //Arrange
                var defaultFilter = new ResponseCloseChatFilterViewModel
                {

                };

                var defaultEntities = GetDefaultResponseCloseChatViewModel()
                                        .AsQueryable();
                //.Where(p=> );

                var expectedEntities = new List<ResponseCloseChatViewModel>
                {
                    new ResponseCloseChatViewModel
                    {
                        Id = 1,
                        //Name = "ResponseCloseChat Name Test 2",
                        //Enabled = true,
                    },
                    new ResponseCloseChatViewModel
                    {
                        Id = 2,
                        //Name = "ResponseCloseChat Name Test 1",
                        //Enabled = true,
                    }
                };

                ServiceMock.Setup(p => p.GetByFilter(defaultFilter, CancellationTokenSource.Token))
                   .ReturnsAsync(defaultEntities)
                   .Verifiable();

                //Act

                var result = await ControllerUnderTest.GetByFilter(defaultFilter, CancellationTokenSource.Token);

                //Assert
                ServiceMock.Verify();

                var okResult = Assert.IsType<OkObjectResult>(result);
                var entitiesResult = Assert.IsAssignableFrom<IEnumerable<ResponseCloseChatViewModel>>(okResult.Value).ToArray();

                Assert.Collection(entitiesResult,
                  entityResult => Assert.Equal(expectedEntities[0], entitiesResult[0]),
                  entityResult => Assert.Equal(expectedEntities[1], entitiesResult[1]));
            }

        }

    }
}
