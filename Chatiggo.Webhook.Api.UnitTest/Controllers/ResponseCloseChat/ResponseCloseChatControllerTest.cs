using Microsoft.AspNetCore.Mvc;
using Moq;
using Chatiggo.Webhook.Api.Controllers;
using Chatiggo.Webhook.Api.Exceptions;
using Chatiggo.Webhook.Api.Services.IServices;
using Chatiggo.Webhook.Api.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Chatiggo.Webhook.Api.UnitTest.Controllers
{
    public partial class ResponseCloseChatControllerTest
    {
        protected ResponseCloseChatController ControllerUnderTest;
        protected Mock<IResponseCloseChatService> ServiceMock;

        public ResponseCloseChatControllerTest()
        {
            ServiceMock = new Mock<IResponseCloseChatService>();
            ControllerUnderTest = new ResponseCloseChatController(ServiceMock.Object);
        }

        public List<ResponseCloseChatViewModel> GetDefaultResponseCloseChatViewModel()
        {
            return new List<ResponseCloseChatViewModel>
            {
                new ResponseCloseChatViewModel
                {
                    Id = 1,
                    //Name = "ResponseCloseChat Name Test 2",
                    //Enabled = true,
                },
                new ResponseCloseChatViewModel
                {
                    Id = 2,
                    //Name = "ResponseCloseChat Name Test 1",
                    //Enabled = true,
                }
            };
        }
    }
}
