using Microsoft.AspNetCore.Mvc;
using Moq;
using Chatiggo.Webhook.Api.Controllers;
using Chatiggo.Webhook.Api.Exceptions;
using Chatiggo.Webhook.Api.Services.IServices;
using Chatiggo.Webhook.Api.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Xunit;

namespace Chatiggo.Webhook.Api.UnitTest.Controllers
{
    public partial class ResponseCloseChatControllerTest
    {
        public class Post : ResponseCloseChatControllerTest
        {
            protected CancellationTokenSource CancellationTokenSource;

            public Post() : base()
            {
                CancellationTokenSource = new CancellationTokenSource();
            }

            [Fact]
            public async Task Should_return_Accepted_result_whit_default_entity_added()
            {
                //Arrange

                var defaultEntity = GetDefaultResponseCloseChatViewModel()[0];

                var expectedEntity = new ResponseCloseChatViewModel
                {
                    Id = 1,
                    //Name = "ResponseCloseChat Name Test 2",
                    //Enabled = true,
                };


                ServiceMock.Setup(p => p.Save(defaultEntity, CancellationTokenSource.Token))
                    .ReturnsAsync(defaultEntity)
                    .Verifiable();

                //Act

                var result = await ControllerUnderTest.Post(defaultEntity, CancellationTokenSource.Token);

                //Assert
                ServiceMock.Verify();

                var acceptedResult = Assert.IsType<AcceptedResult>(result);
                var entityResult = Assert.IsAssignableFrom<ResponseCloseChatViewModel>(acceptedResult.Value);

                Assert.Equal(expectedEntity, entityResult);
            }

        }

    }
}
