using Microsoft.AspNetCore.Mvc;
using Moq;
using Chatiggo.Webhook.Api.ViewModels;
using System.Threading.Tasks;
using System.Threading;
using Xunit;

namespace Chatiggo.Webhook.Api.UnitTest.Controllers
{
    public partial class ResponseCloseChatControllerTest
    {
        public class Put : ResponseCloseChatControllerTest
        {
            protected CancellationTokenSource CancellationTokenSource;

            public Put() : base()
            {
                CancellationTokenSource = new CancellationTokenSource();
            }

            [Fact]
            public async Task Should_return_Accepted_result_whit_default_entity_edited()
            {
                //Arrange

                var defaultEntity = GetDefaultResponseCloseChatViewModel()[0];
                var defaultEntityId = 1;

                var expectedEntity = new ResponseCloseChatViewModel
                {
                    Id = 1,
                    //Name = "ResponseCloseChat Name Test 2",
                    //Enabled = true,
                };


                ServiceMock.Setup(p => p.Update(defaultEntityId, defaultEntity, CancellationTokenSource.Token))
                    .ReturnsAsync(defaultEntity)
                    .Verifiable();

                //Act

                var result = await ControllerUnderTest.Put(defaultEntityId, defaultEntity, CancellationTokenSource.Token);

                //Assert
                ServiceMock.Verify();

                var acceptedResult = Assert.IsType<AcceptedResult>(result);
                var entityResult = Assert.IsAssignableFrom<ResponseCloseChatViewModel>(acceptedResult.Value);

                Assert.Equal(expectedEntity, entityResult);
            }

        }

    }
}
