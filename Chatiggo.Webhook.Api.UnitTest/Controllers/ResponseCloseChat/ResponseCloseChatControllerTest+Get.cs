using Microsoft.AspNetCore.Mvc;
using Moq;
using Chatiggo.Webhook.Api.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using Xunit;

namespace Chatiggo.Webhook.Api.UnitTest.Controllers
{
    public partial class ResponseCloseChatControllerTest
    {
        public class Get : ResponseCloseChatControllerTest
        {
            protected CancellationTokenSource CancellationTokenSource;

            public Get() : base()
            {
                CancellationTokenSource = new CancellationTokenSource();
            }

            [Fact]
            public async Task Should_return_Ok_result_whit_default_entities()
            {
                //Arrange

                var defaultEntities = GetDefaultResponseCloseChatViewModel().AsQueryable();

                var expectedEntities = new List<ResponseCloseChatViewModel>
                {
                    new ResponseCloseChatViewModel
                    {
                        Id = 1,
                        //Name = "ResponseCloseChat Name Test 2",
                        //Enabled = true,
                    },
                    new ResponseCloseChatViewModel
                    {
                        Id = 2,
                        //Name = "ResponseCloseChat Name Test 1",
                        //Enabled = true,
                    }
                };

                ServiceMock.Setup(p => p.GetAll(CancellationTokenSource.Token))
                    .ReturnsAsync(defaultEntities)
                    .Verifiable();

                //Act

                var result = await ControllerUnderTest.Get(CancellationTokenSource.Token);

                //Assert
                ServiceMock.Verify();

                var okResult = Assert.IsType<OkObjectResult>(result);
                var entitiesResult = Assert.IsAssignableFrom<IEnumerable<ResponseCloseChatViewModel>>(okResult.Value).ToArray();

                Assert.Collection(entitiesResult,
                  entityResult => Assert.Equal(expectedEntities[0], entitiesResult[0]),
                  entityResult => Assert.Equal(expectedEntities[1], entitiesResult[1]));
            }

            [Fact]
            public async Task Should_return_Ok_result_whit_default_entity_by_id()
            {
                //Arrange

                var defaultEntity = GetDefaultResponseCloseChatViewModel()[0];
                var defaultEntityId = 1;
                var expectedEntity = new ResponseCloseChatViewModel
                {
                    Id = 1,
                    //Name = "ResponseCloseChat Name Test 2",
                    //Enabled = true,
                };

                ServiceMock.Setup(p => p.GetById(defaultEntityId, CancellationTokenSource.Token))
                    .ReturnsAsync(defaultEntity)
                    .Verifiable();

                //Act

                var result = await ControllerUnderTest.Get(defaultEntityId, CancellationTokenSource.Token);

                //Assert
                ServiceMock.Verify();

                var okResult = Assert.IsType<OkObjectResult>(result);
                var entityResult = Assert.IsAssignableFrom<ResponseCloseChatViewModel>(okResult.Value);

                Assert.Equal(expectedEntity, entityResult);
            }
        }

    }
}
