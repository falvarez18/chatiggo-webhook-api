using Chatiggo.Webhook.Api.Exceptions;
using Chatiggo.Webhook.Api.Model.Classes;
using Chatiggo.Webhook.Api.Repositories;
using Chatiggo.Webhook.Api.Repositories.IRepositories;
using Chatiggo.Webhook.Api.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Xunit;

namespace Chatiggo.Webhook.Api.UnitTest.Repositories
{
    public partial class ResponseCloseChatRepositoryTest : BaseRepositoryTest
    {
        public class Delete : ResponseCloseChatRepositoryTest
        {
            protected CancellationTokenSource CancellationTokenSource;

            public Delete() : base()
            {
                CancellationTokenSource = new CancellationTokenSource();
            }

            [Fact]
            public async Task Should_return_default_entity_deleted()
            {
                //Arrange
                InMemoryDbContext.ResponseCloseChat.Add(GetDefaultResponseCloseChat()[0]);
                InMemoryDbContext.SaveChanges();

                var defaultEntityId = 1;

                var expectedEntity = new ResponseCloseChatViewModel
                {
                    Id = 1,
                    //Name = "ResponseCloseChat Name Test 2",
                    //Enabled = true,
                };

                //Act

                var entityResult = await RepositoryUnderTest.Delete(defaultEntityId, CancellationTokenSource.Token);

                //Assert
                Assert.NotNull(entityResult);
                Assert.True(entityResult.Id > 0);
                //Assert.Equal(expectedEntity.Name, entityResult.Name);
                //Assert.Equal(expectedEntity.Enabled, entityResult.Enabled);
            }


            [Fact]
            public async Task Should_throw_EntityToDeleteNotFoundChatiggoWebhookException_for_inexistent_entity()
            {
                //Arrange
                var defaultEntityId = 1;

                //Act

                //Assert
                await Assert.ThrowsAsync<EntityToDeleteNotFoundChatiggoWebhookException>(async () => { await RepositoryUnderTest.Delete(defaultEntityId, CancellationTokenSource.Token); });

            }

        }

    }
}
