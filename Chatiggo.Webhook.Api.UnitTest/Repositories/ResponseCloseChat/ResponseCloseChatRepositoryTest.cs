using Chatiggo.Webhook.Api.Exceptions;
using Chatiggo.Webhook.Api.Model.Classes;
using Chatiggo.Webhook.Api.Repositories;
using Chatiggo.Webhook.Api.Repositories.IRepositories;
using Chatiggo.Webhook.Api.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Chatiggo.Webhook.Api.UnitTest.Repositories
{
    public partial class ResponseCloseChatRepositoryTest : BaseRepositoryTest
    {
        protected IResponseCloseChatRepository RepositoryUnderTest;

        public ResponseCloseChatRepositoryTest() : base()
        {
            RepositoryUnderTest = new ResponseCloseChatRepository(InMemoryDbContext, IMapper);
        }

        public List<ResponseCloseChat> GetDefaultResponseCloseChat()
        {
            return new List<ResponseCloseChat>
            {
                new ResponseCloseChat
                {
                    Id = 1,
                    //Name = "ResponseCloseChat Name Test 2",
                    //Enabled = 1,
                },
                new ResponseCloseChat
                {
                    Id = 2,
                    //Name = "ResponseCloseChat Name Test 1",
                    //Enabled = 1,
                },
                new ResponseCloseChat
                {
                    Id = 3,
                    //Name = "ResponseCloseChat Name Test 3",
                    //Enabled = 1,
                },
                new ResponseCloseChat
                {
                    Id = 4,
                    //Name = "ResponseCloseChat Name Test 4",
                    //Enabled = 0,
                }

            };
        }


    }
}
