using Chatiggo.Webhook.Api.Exceptions;
using Chatiggo.Webhook.Api.Model.Classes;
using Chatiggo.Webhook.Api.Repositories;
using Chatiggo.Webhook.Api.Repositories.IRepositories;
using Chatiggo.Webhook.Api.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Xunit;

namespace Chatiggo.Webhook.Api.UnitTest.Repositories
{
    public partial class ResponseCloseChatRepositoryTest : BaseRepositoryTest
    {
        public class GetById : ResponseCloseChatRepositoryTest
        {
            protected CancellationTokenSource CancellationTokenSource;

            public GetById() : base()
            {
                CancellationTokenSource = new CancellationTokenSource();
            }

            [Fact]
            public async Task Sould_return_default_entity()
            {
                //Arrange

                var entityToAdd = GetDefaultResponseCloseChat()[0];
                InMemoryDbContext.ResponseCloseChat.Add(entityToAdd);
                InMemoryDbContext.SaveChanges();

                var entityId = 1;
                var expectedEntity = new ResponseCloseChatViewModel
                {
                    Id = 1,
                    //Name = "ResponseCloseChat Name Test 2",
                    //Enabled = true,
                };

                //Act

                var result = await RepositoryUnderTest.GetById(entityId, CancellationTokenSource.Token);

                //Assert
                Assert.Equal(expectedEntity, result);
            }

            [Fact]
            public async Task Sould_return_null()
            {
                //Arrange
                var entityId = -1;

                //Act
                var result = await RepositoryUnderTest.GetById(entityId, CancellationTokenSource.Token);

                //Assert
                Assert.Null(result);
            }
        }


    }
}
