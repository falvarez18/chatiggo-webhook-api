using Chatiggo.Webhook.Api.Exceptions;
using Chatiggo.Webhook.Api.Model.Classes;
using Chatiggo.Webhook.Api.Repositories;
using Chatiggo.Webhook.Api.Repositories.IRepositories;
using Chatiggo.Webhook.Api.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Xunit;

namespace Chatiggo.Webhook.Api.UnitTest.Repositories
{
    public partial class ResponseCloseChatRepositoryTest : BaseRepositoryTest
    {
        public class Save : ResponseCloseChatRepositoryTest
        {
            protected CancellationTokenSource CancellationTokenSource;

            public Save() : base()
            {
                CancellationTokenSource = new CancellationTokenSource();
            }

            [Fact]
            public async Task Should_return_default_entity_added()
            {
                //Arrange

                var defaultEntity = new ResponseCloseChatViewModel
                {
                    //Name = "ResponseCloseChat Name Test 1",
                    //Enabled = true,
                };

                var expectedEntity = new ResponseCloseChatViewModel
                {
                    Id = 1,
                    //Name = "ResponseCloseChat Name Test 1",
                    //Enabled = true,
                };

                //Act

                var entityResult = await RepositoryUnderTest.Save(defaultEntity, CancellationTokenSource.Token);

                //Assert
                Assert.NotNull(entityResult);
                Assert.True(entityResult.Id > 0);
                //Assert.Equal(expectedEntity.Name, entityResult.Name);
                //Assert.Equal(expectedEntity.Enabled, entityResult.Enabled);
            }

            [Fact]
            public async Task Should_throw_ArgumentNullException()
            {
                //Arrange
                ResponseCloseChatViewModel nullEntityToAdd = null;

                //Act

                //Assert
                await Assert.ThrowsAsync<ArgumentNullException>(async () => { await RepositoryUnderTest.Save(nullEntityToAdd, CancellationTokenSource.Token); });

            }
        }

    }
}
