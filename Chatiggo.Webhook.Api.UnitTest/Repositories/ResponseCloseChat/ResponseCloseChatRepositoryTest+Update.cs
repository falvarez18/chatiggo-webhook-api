using Chatiggo.Webhook.Api.Exceptions;
using Chatiggo.Webhook.Api.Model.Classes;
using Chatiggo.Webhook.Api.Repositories;
using Chatiggo.Webhook.Api.Repositories.IRepositories;
using Chatiggo.Webhook.Api.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Xunit;

namespace Chatiggo.Webhook.Api.UnitTest.Repositories
{
    public partial class ResponseCloseChatRepositoryTest : BaseRepositoryTest
    {
        public class Update : ResponseCloseChatRepositoryTest
        {
            protected CancellationTokenSource CancellationTokenSource;

            public Update() : base()
            {
                CancellationTokenSource = new CancellationTokenSource();
            }

            [Fact]
            public async Task Should_return_default_entity_edited()
            {
                //Arrange

                InMemoryDbContext.ResponseCloseChat.Add(GetDefaultResponseCloseChat()[0]);
                InMemoryDbContext.SaveChanges();

                var defaultEntity = new ResponseCloseChatViewModel
                {
                    Id = 1,
                    //Name = "ResponseCloseChat Name Test 1 Edited",
                    //Enabled = true,
                };

                var expectedEntity = new ResponseCloseChatViewModel
                {
                    Id = 1,
                    //Name = "ResponseCloseChat Name Test 1 Edited",
                    //Enabled = true,
                };
                //Act

                var entityResult = await RepositoryUnderTest.Update(defaultEntity, CancellationTokenSource.Token);

                //Assert
                Assert.NotNull(entityResult);
                Assert.Equal(expectedEntity, entityResult);
            }

            [Fact]
            public async Task Should_throw_InvalidOperationException_for_null_entity()
            {
                //Arrange
                ResponseCloseChatViewModel nullEntityToAdd = null;

                //Act

                //Assert
                await Assert.ThrowsAsync<InvalidOperationException>(async () => { await RepositoryUnderTest.Update(nullEntityToAdd, CancellationTokenSource.Token); });

            }

            [Fact]
            public async Task Should_throw_EntityToDeleteNotFoundChatiggoWebhookException_for_inexistent_entity()
            {
                //Arrange
                var entityToEdit = new ResponseCloseChatViewModel
                {
                    Id = -1,
                };

                //Act

                //Assert
                await Assert.ThrowsAsync<EntityToEditNotFoundChatiggoWebhookException>(async () => { await RepositoryUnderTest.Update(entityToEdit, CancellationTokenSource.Token); });

            }
        }

    }
}
