using Chatiggo.Webhook.Api.Exceptions;
using Chatiggo.Webhook.Api.Model.Classes;
using Chatiggo.Webhook.Api.Repositories;
using Chatiggo.Webhook.Api.Repositories.IRepositories;
using Chatiggo.Webhook.Api.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Xunit;

namespace Chatiggo.Webhook.Api.UnitTest.Repositories
{
    public partial class ResponseCloseChatRepositoryTest : BaseRepositoryTest
    {
        public class GetAll : ResponseCloseChatRepositoryTest
        {
            protected CancellationTokenSource CancellationTokenSource;

            public GetAll() : base()
            {
                CancellationTokenSource = new CancellationTokenSource();
            }

            [Fact]
            public async Task Should_return_default_entities()
            {
                //Arrange

                InMemoryDbContext.ResponseCloseChat.AddRange(GetDefaultResponseCloseChat());
                InMemoryDbContext.SaveChanges();

                var expectedEntities = new ResponseCloseChatViewModel[]
                {
                    new ResponseCloseChatViewModel
                    {
                        Id = 1,
                        //Name = "ResponseCloseChat Name Test 2",
                        //Enabled = true
                    },
                     new ResponseCloseChatViewModel
                    {
                        Id = 2,
                        //Name = "ResponseCloseChat Name Test 1",
                        //Enabled = true
                    },
                    new ResponseCloseChatViewModel
                    {
                        Id = 3,
                        //Name = "ResponseCloseChat Name Test 3",
                        //Enabled = true
                    },
                    new ResponseCloseChatViewModel
                    {
                        Id = 4,
                        //Name = "ResponseCloseChat Name Test 4",
                        //Enabled = false
                    },
                };

                //Act

                var result = await RepositoryUnderTest.GetAll(CancellationTokenSource.Token);

                //Assert

                var entitiesResult = result.ToArray();
                //Assert
                Assert.Collection(entitiesResult,
                  entityResult => Assert.Equal(expectedEntities.ElementAt(0), entitiesResult[0]),
                  entityResult => Assert.Equal(expectedEntities.ElementAt(1), entitiesResult[1]),
                  entityResult => Assert.Equal(expectedEntities.ElementAt(2), entitiesResult[2]),
                  entityResult => Assert.Equal(expectedEntities.ElementAt(3), entitiesResult[3])
                  );
            }
        }

    }
}
